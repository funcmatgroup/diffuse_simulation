"""
Randomly generates diffuse scattering models from arbitrary crystal structures

Designed to read in structural data from a CIF file, (randomly) create correlations
between pairs of atoms and then produce a macro for DISCUS to perform in order
to generate diffuse scattering
"""

__author__ = "James Cumby"
__email__ = "james.cumby@ed.ac.uk"

import os
import numpy as np
from tqdm import tqdm
import json
from itertools import combinations

from pymatgen.core.structure import Structure
from pymatgen.util.coord import get_angle
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer


# Set up NumPy random numbers
rng = np.random.default_rng()



class Disordered_Crystal():
    """ Crystal with disordered correlations """

    def __init__(self,
                 structure,
                 filename = 'discus',
                 crystal_dim = 200,
                 fourier = {'how': 'fixed_size',
                            'min_res': 1.2,
                            'grid_step': 0.0,
                            'grid_size': [101, 101, 101]
                            },
                 outdir = "."
                 ):
        """ Initialise disordered crystal from pymatgen structure


        Parameters
        ----------
        fourier : dict
            Defintion of how to set up the Fourier calculation grid (default fixed size with Fourier-safe step)

            'how' : str
                Define either
                    - 'fixed_size' where the number of voxels is fixed (must also specify 'grid_size') or
                    - 'fixed_res' where the grid is expanded to meet the minimus resolution (must also
                       specify 'min_res')

            Whether to generate a fourier grid of fixed size ('size') or resolution ('res').
            If size, the resolution depends on lattice parameter, and is a multiple of 1/ ncells * recip_lattice
            (multiple chosen to be at least as large as fou_grid).
            If 'res', grid is extended to minimum resolution in units of fou_grid.

        """
        self.structure = structure

        self.filename = filename
        self.outdir = outdir
        self.neighbourhoods = []

        #self.min_res = min_res

        #cell_multiplier is always even (for negative correlations)
        self.cell_multiplier = self.find_crystal_scale(size = crystal_dim)

        # Defines self.fou_step, self.fou_max, self.grid_size and self.hkl_max
        self.define_fourier(fourier)

        #self.fourier_multiplier = self.find_fourier_scale()



        # Determine best voxel size to minimise Fourier ringing
        #approx_res = np.ones(3) * 0.02
        #ideal_res = 1.0 / self.cell_multiplier * self.structure.lattice.reciprocal_lattice_crystallographic.abc

        #self.fou_grid = 0.02

    def define_fourier(self, four_dict):
        """ Set up the fourier defintion """

        assert 'how' in four_dict.keys()

        # If we have specified a step, use it, otherwise calculate minimum to avoid Fourier truncation
        if four_dict.get('grid_step', 0) > 0:
            fou_step = np.ones * four_dict['grid_step']
        else:
            fou_step = 1 / self.cell_multiplier * self.structure.lattice.reciprocal_lattice_crystallographic.abc


        if four_dict['how'] == 'fixed_size':
            # Determine resolution based on size of Fourier grid
            assert 'grid_size' in four_dict, 'You must specify grid_size to use "how" = "fixed_size"'
            grid_size = np.array(four_dict['grid_size'])
            assert (grid_size % 2 != 0).all(), 'An odd-numbered grid must be chosen to account for centering about the origin'

            #originally divided by 2.0, changed to 4.0 when crystal made larger
            #nsteps = np.ceil(grid_size / 2.0)

            # Determine number of steps in positive (or negative) direction
            nsteps_pos = grid_size // 2
            fou_max = fou_step * nsteps_pos
            if four_dict.get('min_res'):

                # If min_res has been specified, try to multiply fou_step to meet it
                if four_dict.get('grid_step', 0) == 0:
                    while (fou_max < 1 / four_dict['min_res']).all():
                        fou_step += fou_step
                        fou_max = fou_step * nsteps_pos
                else:
                    raise ValueError(f'Cannot satisfy min_res ({four_dict[min_res]}) given grid_step ({four_dict[grid_step]} and fixed Fourier size. \nConsider setting min_res = 0 to ignore')
        elif four_dict['how'] == 'fixed_res':
            assert 'min_res' in four_dict.keys(), 'min_res must be specified if "how" == "fixed_res"'

            nsteps = np.ceil(np.ones(3) * 1. / four_dict['min_res'] / fou_step)
            grid_size = nsteps * 2 + 1

            fou_max = nsteps * fou_step


        self.fou_step = fou_step
        self.fou_max = fou_max
        self.fou_grid_size = grid_size

        self.hkl_max = fou_max / self.structure.lattice.reciprocal_lattice_crystallographic.abc





    @classmethod
    def from_CIF(cls, cif, outdir = ".", inverse_state = 'included'):
        """ Create a Disordered_Crystal from a CIF representation

        Arguments
        ---------
        cif : str
            Either a location of a CIF file or a raw CIF string

        Returns
        -------
        (Disordered_Crystal)
            self.filename is set based on the input CIF file (taken from 'data_NAME'
            in the case of a string input).
        """

        assert isinstance(cif, str)
        #print(os.path.abspath(cif))
        if os.path.exists(os.path.abspath(cif)):
            struct = Structure.from_file(cif)
            name = os.path.splitext(os.path.basename(cif))[0]# + "_" + inverse_state
            #print("filename: " + name)
        else:
            assert 'data_' in cif, "Valid CIF files must contain a 'data_' block"
            start = cif.index('data_')
            name = cif[start: start+100].split()[0]
            name = name[5:] + "_" + inverse_state
            struct = Structure.from_str(cif, fmt='cif')

        return cls(struct, filename = name, outdir = outdir)


    def multi_occupancy_sites(self, structure = None):
        """ Return a substructure of partially-occupied sites where present, otherwise the whole structure.

        By 'partially occupied', this includes:
            - sites with partial occupancy (i.e. vacancies)
            - mixed-sites (occpancies sum to 1 over multiple atoms)

        """

        if structure == None:
            structure = self.structure

        occ_sites = []
        for site in structure:
            if len(site.species) > 1:
                occ_sites.append(site)
        
        if len(occ_sites) > 0:
            # Just return the sites with non-integer occupancies
            substructure = Structure.from_sites(occ_sites)
        else:
            # Return the whole crystal
            substructure = structure

        return substructure


    def equiv_occupancy_sites(self, reference_site, structure = None, threshold=0.05):
        """ Find all sites within a structure that share a common chemistry with `reference_site`

        This returns only the sites that have the same element(s) with the same
        fractional stoichiometry (within a threshold of 5%).

        TODO: extend this algorithm to consider site symmetry or local coordination.
        """

        if structure == None:
            structure = self.structure

        # Convert site.species to dict() with .as_dict().
        #Compare element by element so comp diff is below some threshold (5%)
        occ_sites = []
        ref_dict = reference_site.species.as_dict()

        for site in structure:
            site_dict = site.species.as_dict()
            for key in site_dict:
                if abs(site_dict[key] - ref_dict[key]) <= threshold:
                    occ_sites.append(site)
        substructure = Structure.from_sites(occ_sites)
        return substructure


    def _vector_correlation(self, structure = None, start_atom = None, dim = None, ndim = None):
        """ Generate a random correlation vector.

        Parameters
        ----------
        start_atom : pymatgen periodic site
            Atom to use as starting point of vector (default random choice)

        Returns
        -------
        [atom1, atom2, vec, corr]
            List of two periodic sites, the unit cell displacement between them and the correlation
            coefficient
        """

        if structure == None:
            structure = self.structure
        #fix it so separate atoms are picked everytime???
        atom1, atom2 = rng.choice(structure, size=2, replace=True)
        if start_atom is not None:
            atom1 = start_atom

        """
        if dim == 0:
            vec = np.array([1,0,0])
        elif dim == 1:
            vec = np.array([0,1,0])
        """

        vec = rng.choice([-1,0,1], size=3, replace=True)
        #vec = np.array([1, 0, 0])
        # Check we are not looking at the same atom in the same position
        # Also check that only equivalent occupancy sites are being mixed up
        """THIS IS THE PROBLEM AFTER THE 'OR' - maybe doesn't need a fix if
        proper structure implemented every time"""
        while ((vec == [0,0,0]).all() and atom1 == atom2): # or atom1.species != atom2.species:
            vec = rng.choice([-1,0,1], size=3, replace=True)

        if dim < ndim:
            corr = rng.choice([-1, 1]) 
            #corr = 1
        else:   corr = 0

        return [atom1, atom2, vec, corr]

    def _get_cosine(self,
                    atom1,
                    atom2,
                    atom3,
                    atom1_image = [0,0,0],
                    atom2_image = [0,0,0],
                    atom3_image = [0,0,0],
                    structure = None
                    ):
        """ Return the cosine of the angle atom1-atom2-atom3. """

        #return 0.2
        if structure == None:
            structure = self.structure

        atom1pos = structure.lattice.get_cartesian_coords(atom1.frac_coords + atom1_image)
        atom2pos = structure.lattice.get_cartesian_coords(atom2.frac_coords + atom2_image)
        atom3pos = structure.lattice.get_cartesian_coords(atom3.frac_coords + atom3_image)

        v1 = atom1pos - atom2pos
        v2 = atom3pos - atom2pos
        d = np.dot(v1, v2) / np.linalg.norm(v1) / np.linalg.norm(v2)
        d = min(d, 1)
        d = max(d, -1)

        return d

    def _inverse_correlation(self, corr_vec, structure = None):
        """ Find the opposite correlation vector to an existing definition."""

        # # Determine if we're centrosymmetric
        # centrosym = SpacegroupAnalyzer(self.structure).is_laue()


        # if centrosym and (corr_vec[2] != np.zeros(3)).any():
            # # Just take the negative jimage
            # return [corr_vec[0], corr_vec[1], -corr_vec[2], corr_vec[3]]
        # else:


        if structure == None:
            structure = self.structure

        v1 = corr_vec[1].coords + np.dot(corr_vec[2], structure.lattice.matrix) - corr_vec[0].coords
        v1_dist = np.linalg.norm(v1)

        # Correlation within a unit cell: we need to find the closest 'opposite'
        # (or close to opposite for non-centrosymmetric)
        best_angle = 0
        best_jimage = np.zeros(3)
        for dx in [-2,-1,0,1,2]:
            for dy in [-2,-1,0,1,2]:
                for dz in [-2,-1,0,1,2]:
                    if (corr_vec[2] == np.array([dx,dy,dz])).all():
                        # Skip existing jimage
                        continue

                    # Calculate interatomic vectors (cartesian)

                    v2 = corr_vec[1].coords + np.dot([dx,dy,dz], structure.lattice.matrix) - corr_vec[0].coords
                    angle = get_angle(v1, v2)
                    #print([dx,dy,dz], v1, v2, angle)

                    if angle > best_angle and np.isclose(np.linalg.norm(v2), v1_dist):
                        best_angle = angle
                        best_jimage = np.array([dx,dy,dz])

        # Correct for excessive displacement
        if (best_jimage % 2 == np.zeros(3)).all():
            best_jimage = best_jimage / 2

        return [corr_vec[0], corr_vec[1], best_jimage, corr_vec[3]]

    def random_correlation(self,
                           structure = None,
                           ndim = 1,
                           corr_type = 'random',
                           inverse_state = "included"
                           ):
        """ Add a random correlation to self

        Parameters
        ----------
        ndim : int
            Number of dimensions (non-parallel interatomic vectors)
            over which correlation should occur
            (1,2,3, default = 1)
        corr_type : str
            Type of correlation to perform:
                - 'occ' : occupancy-based
                - 'disp' : displacement-based
                - 'random' : choice of occupancy or displacement

        """

        if corr_type == 'random':
            corr_type = rng.choice(['occ','disp'])

        if structure == None:
            structure = self.structure


        # Collect related vectors into an env(ironment), and
        # store these in a neigh (to avoid testing existing self.neighbourhoods)
        neigh = []
        env = []


        #while True:
        #    new_vec = self._vector_correlation()
        #    if corr_type == 'occ' and new_vec[0].species_string == new_vec[1].species_string:
        #        # Don't perform occupancy correlation for smae species
        #        continue
        #    break


        """automatically make 3 vectors, just stress that those that aren't part of 'ndim'
        forced to be 0 (so no unwanted correlations)"""

        #may want to fix the start atom
        start_atom = None

        for d in range(3):
            env = []
            i = 0
            while True:
                new_vec = self._vector_correlation(structure = structure, start_atom = start_atom, dim = d, ndim = ndim)
                cosines = []
                if len(neigh) > 0 and new_vec[1] != neigh[0][0][1]:
                    continue

                for n in neigh:
                    for e in n:
                        cosines.append(self._get_cosine(new_vec[1],
                                            new_vec[0],
                                            e[1],
                                            new_vec[2],
                                            np.zeros(3),
                                            e[2],
                                            structure
                                            ))
                if (np.abs(np.array(cosines)) < 0.9).all():
                    # Only jump out of loop if vectors are nearly orthogonal
                    break
                if i > 1E6:
                    raise RuntimeError("Maximum number of vector guesses exceeded ({})".format(i))
                i+= 1

            start_atom = new_vec[0]
            env.append(new_vec + [corr_type])

            """ "inverse_state" determines what we do with the inverse_vectors
                -    if inverse_state == "included", its included in the same
                    environment as the other vectors
                -    if inverse_state == "separate", its included but in a
                separate environment with different target correlations
                -    if its neither, then the inverse vectors are ignored.
            """
            if inverse_state == "included":
                env.append(self._inverse_correlation(new_vec, structure) + [corr_type])
            elif inverse_state == "separate":
                inv_env = []
                inv_env.append(self._inverse_correlation(new_vec, structure) + [corr_type])

            #print("env: ", env)
            neigh.append(env)
            if inverse_state == "separate":
                #print("inv_env: ", inv_env)
                neigh.append(inv_env)


        for n in neigh:
            self.neighbourhoods.append(n)

        # # Select first (1D) vector and inverse
        # new_vec = self._vector_correlation()
        # env.append(new_vec + [corr_type])
        # env.append(self._inverse_correlation(new_vec) + [corr_type])

        # neigh.append(neigh)
        # env = []

        # # Choose increasing number of vectors (could be > 3, but might break collinearity)
        # for d in range(ndim-1):
            # cosines = np.ones(len(neigh))
            # while (cosines > 0.9).any():
                # new_vec = self._vector_correlation(start_atom = neigh[0][0])

                # # If abs(cosine) > 0.9, vectors are deemed collinear
                # for i, n in enumerate(neigh):
                    # # Check that we're using the same atom pairs throughout
                    # # (necessary for Discus environments)
                    # if new_vec[1].species_string != n[1].species_string:
                        # cosines = np.ones(len(neigh))
                        # continue
                    # cosines[i] = (np.abs(self._get_cosine(new_vec[1],
                                                          # new_vec[0],
                                                          # n[1],
                                                          # new_vec[2],
                                                          # np.zeros(3),
                                                          # n[2],
                                                          # )))

            # neigh.append(new_vec + [corr_type])
            # neigh.append(self._inverse_correlation(new_vec) + [corr_type])

        # self.neighbourhoods.append(neigh)

    def random_neighbourhood(self, ndim, corr_type = 'random', structure = None, inverse = 'included'):
        """ Will run through random_correlation for different substructures where
        only random occupancy sites are considered"""
        if structure == None:
            structure = self.structure

        assert ndim > 0 and ndim < 4

        if corr_type == 'random':
            corr_type = rng.choice(['occ','disp'])

        """ #METHOD 1:
        #Only considers correlations between equivalent crystallographic sites
        for site in structure:
            substructure = Structure.from_sites([site])
            cryst.random_correlation(substructure, ndim, corr_type)
        """
        """
        #METHOD 2:
        #Can correlate between sites of equivalent occupancy. Does this for
        #every multi_occ_site in the structure
        occ_types = []
        for site in structure:
            if occ_types.count(site.species) == 0:
                substructure = cryst.equiv_occupancy_sites(site, structure)
                occ_types.append(site.species)
            cryst.random_correlation(substructure, ndim, corr_type, inverse)
        """

        #METHOD 3:
        #Only correlate a atoms on sites with equivalent occupancy. The substructure
        #passed only includes these sites.
        cryst.random_correlation(structure, ndim, corr_type, inverse)

    def find_crystal_scale(self, size = 50):
        """ Determine the unit cell multiplier to achieve a (roughly) cubic crystal """

        multiplier = np.ones(3) * size / self.structure.lattice.lengths

        if np.ceil(multiplier).all() % 2 == 1:
            multiplier += 1
        # Use ceiling to ensure crystal is at least 50 Ang.
        return np.ceil(multiplier)

    def find_fourier_scale(self):
        """ Find maximum h,k,l indices to resolve diffraction to min_res. """

        multiplier = np.ones(3) * self.min_res / self.structure.lattice.reciprocal_lattice_crystallographic.lengths

        return np.ceil(multiplier)

    def max_index(self, site):
        """ Finds the index of the most common element at a crystallographic site. """
        return np.array(site.species.as_dict().values()).argmax()

    def find_site_label(self, site):
        """Returns the name of the most common element at a crystallographic site."""

        return site.species.elements[self.max_index(site)].symbol

    def write_discus_cell(self):
        """ Write a discus filename.cell file to ensure atom ordering. """
        outfile = os.path.join(self.outdir, self.filename + ".cell")
        with open(outfile, 'w') as f:
            f.write(f'title {self.filename}\n')
            f.write('spcgr P1\n')
            f.write('cell {}, {}, {}, {}, {}, {}\n'.format(*self.structure.lattice.parameters))
            f.write('atoms\n')
            for i, atom in enumerate(self.structure):
                f.write('{:5s}{:.6f}, {:.6f}, {:.6f}, {:.6f}\n'.format(self.find_site_label(atom), atom.a, atom.b, atom.c, 0.8))

    def generate_VOID_substitution_string(self):
        """ Find if any occupancy neighbourhoods are for the same species, and if so replace them with voids.

        Returns
        -------
        substring : str
            Discus string to set replacements

        """
        substring = ""
        if len(self.neighbourhoods) == 0:
            return substring

        to_sub = []
        for n in self.neighbourhoods:
            if n[0][4] != 'occ':
                continue
            if n[0][0].species_string == n[0][1].species_string:
                # Need to substitute (just work with 'parent' atom, and let mmc move things
                if n[0][0] not in to_sub:
                    to_sub.append(n[0][0])


        for spec in to_sub:
            substring += "replace {}, VOID, {}, {:.3f}\n".format(spec.species_string, self.structure.index(spec)+1, rng.random())

        return substring

    def generate_ATOM_substitution_string(self, site):
        """ Return DISCUS line(s) to substitute mixed occupancy atoms. """

        #see if you can add substitutions to a sepcific site

        #sub in a particular site with other elements on that site with a given occupancy
        #make sure only the most common elements is subbed and not minor replacements
        #Sub only on a given site in the structure
        #substring = ""
        #i = 0
        #for site in self.structure:
        #    i += 1
        #    main_atom = self.find_site_label(site)   #identify the most common element at this site
        #    #site number in DISCUS = site in structure
        #    prob_main = 1.0
        #    for elem in site.species.elements:  #cycle through the list of elements at this site
        #        if elem.name != main_atom:
        #            if prob_main == 1.0:
        #                # replace, <at1>, <at2>, <site>, <prob>
        #                prob = site.species.remove_charges().as_dict()[elem.name]  #returns the relative composition of 'elem.name'
        #                prob_main -= prob
        #            else:
        #                prob = site.species.remove_charges().as_dict()[elem.name]/prob_main
        #                prob_main -= prob
        #            substring += "replace {}, {}, {}, {:.3f}\n".format(main_atom, elem.name, i, prob)#


        substring=""

        main_atom = self.find_site_label(site)   #identify the most common element at this site
        #site number in DISCUS = site in structure
        site_number = self.structure.index(site)+1

        # Find number of voids for partially-occupied sites
        void_frac = 1-site.species.num_atoms
        assert void_frac >= 0, "Site occupancy cannot be greater than 1.0!"

        prob_main = 1.0
        for elem in site.species.elements:  #cycle through the list of elements at this site
            prob = site.species.remove_charges().as_dict()[elem.symbol]/prob_main
            
            if elem.symbol == main_atom:
                if void_frac > 0:
                    prob = void_frac / prob_main
                    substring += "replace {}, {}, {}, {:.3f}\n".format(main_atom, "VOID", site_number, prob)
                else:
                    # Avoid incrementing prob_main if just looking at main atom
                    continue
            else:
                substring += "replace {}, {}, {}, {:.3f}\n".format(main_atom, elem.symbol, site_number, prob)

            prob_main -= prob

        #print(substring)
        return substring



    def discus_generate_string(self, save_stru = False, save_cif = False):
        """ Generate macro string for reading in and configuring DISCUS """

        instr =  "discus\n"
        #instr += "\nseed 10\n"
        instr += "read\n"
        instr += f"cell {self.filename+'.cell'}, {self.cell_multiplier[0]}, {self.cell_multiplier[1]}, {self.cell_multiplier[2]}\n"
        instr += "\n"


        # Check if we need to perform any substitutions for 'occ' between equivalent species
        # %HERE% -check for multiple occupancy sites, then choose btw ATOM or VOID functions
        multi_occ = False   #TRUE if multiple occupancies are present
        for site in self.structure:
            if len(site.species) > 1 or site.species.num_atoms < 1.0:
                instr += self.generate_ATOM_substitution_string(site)
                # Currently not using random VOID substitution
                #instr += self.generate_VOID_substitution_string()

        # Add thermal displacement randomly (used for displacement correlations)
        if 'disp' in [n[0][4] for n in self.neighbourhoods]:
            instr += "ther all\n\n"

        if len(self.neighbourhoods) > 0:
            instr += "mmc\n"
            instr += "set neigh, reset\n\n"

        # Generate discus vectors and neighbourhoods
        occ_envs = []
        disp_envs = []
        v = 1
        for k, nhood in enumerate(self.neighbourhoods):

            for vec in nhood:
                at1_idx = self.structure.index(vec[0])+1
                at2_idx = self.structure.index(vec[1])+1
                instr += f"set vec, {v}, {at1_idx}, {at2_idx}, {vec[2][0]}, {vec[2][1]}, {vec[2][2]}\n"
                v += 1
            instr += "set neigh, vec, {}\n".format(", ".join([str(y) for y in range(v - len(nhood), v)]))
            instr += "\n"
            instr += "set neigh, add\n"

            if vec[4] == 'occ':
                occ_envs.append((k, nhood))
            elif vec[4] == 'disp':
                disp_envs.append((k, nhood))

        # OCC commands
        if len(occ_envs) > 0:
            instr += "# Perform substitutions\n"
            instr += "set mode, 1.0, swchem, all\n"
            #k = 0
            """groups atoms if more than 2 atoms on a site, otherwise normal
            also changed to ENER mode -> need to define ising energy now rather
            than correlation"""
            for k, nhood in occ_envs:
                if nhood[0][3] == 0:
                    continue

                """
                if len(nhood[0][0].species) > 2:
                    spec1 = nhood[0][0].species.elements[0].name
                    indices = list(range(1, len(nhood[0][1].species)))
                    group = []
                    for index in indices:
                            group.append(nhood[0][1].species.elements[index].name)
                    group = tuple(group)

                    instr += "set target, {}, corr, {}, {}, {}, {:.4f}, ENER\n".format(k+1, spec1, group, 0.0, nhood[0][3])
                else:
                    spec1 = nhood[0][0].species.elements[0].name
                    spec2 = nhood[0][0].species.elements[1].name
                    instr += "set target, {}, corr, {}, {}, {}, {}, ENER\n".format(k+1, spec1, spec2, 0.0, nhood[0][3])
                """
                # Currently correlates atom 1 with atom 2
                # What if more than 2 atoms on a site???
                # Use all combinations of elements with the same correlation?
                combin = list(combinations(np.arange(1, len(nhood[0][0].species) + 1), 2))
                #print("combinations: ", combin)
                for pair in combin:
                    spec1 = nhood[0][0].species.elements[pair[0] - 1].symbol
                    spec2 = nhood[0][1].species.elements[pair[1] - 1].symbol
                    # if species are the same, occ is between voids
                    if spec1 == spec2:
                        spec2 = 'VOID'
                    instr += "set target, {}, corr, {}, {}, {:.4f}, {}, CORR\n".format(k+1, spec1, spec2, nhood[0][3], 0.0)

                    #k += 1

            # Set up general mmc occ cycle behaviour
            instr += "\nset cyc, 200*n[1]\nset feed, 10*n[1]\nset temp, 1.0\n\n"
            instr += "run\n"

            instr += "exit\n"
            #generates a string to help with the analysis of the structure
            instr += self.discus_generate_analysis(occ_envs)


        # DISPLACEMENT commands
        if len(disp_envs) > 0:
            instr += "mmc\n"
            instr += "# Perform displacements\n"
            instr += "set mode, 1.0, swdisp, all\n"

            for k, nhood in disp_envs:
                spec1 = nhood[0][0].species_string
                spec2 = nhood[0][1].species_string
                # Define the correlated displacement
                # (does not consider distances, only displacement from average position)
                instr += "set target, {}, cd, {}, {}, {:.4f}, {}, CORR\n".format(k+1, spec1, spec2, nhood[0][3], 0.0)

            # Set up general mmc DISP cycle behaviour
            #instr += "set move, all, 0.01, 0.01, 0.01\n"

            instr += "\nexit\n"

        if save_stru:
            instr += "save {}\n".format(self.filename + '.stru')

        if save_cif:
            instr += "plot\n"
            instr += "program cif\n"
            instr += "select all\n"
            instr += "outfile {}\n".format(self.filename + "_generated.cif")
            instr += "run\n"

            instr += "\nexit\n"

        return instr


    def discus_generate_analysis(self, occ_envs):
        #only returns the correlation values along defined vectors.
        instr = "\n"
        instr += "chem\n\n"

        #instr += "set lots, box, 2, 2, 2, 500, y\n"
        #instr += "set bins, 11\n"

        used_pairs = []
        for k, nhood in occ_envs:
            """
            if len(nhood[0][0].species) > 2:
                spec1 = nhood[0][0].species.elements[0].name
                indices = list(range(1, len(nhood[0][1].species)))
                group = []
                for index in indices:
                        group.append(nhood[0][1].species.elements[index].name)
                group = tuple(group)
                field_File = self.filename + "_field_" + str(spec1) + group) + ".txt"
                homo_cor_File = self.filename + "_homo_cor_" + str(spec1) + group + ".txt"
                instr += "field occ, {}, {}, {}, {}, {}\n".format(spec1, group, field_File, '0', '9')
                instr += "#homo cor, \"occ\", {}, {}, {}\n".format(spec1, group, homo_cor_File)
            else:
                spec1 = nhood[0][0].species.elements[0].name
                spec2 = nhood[0][0].species.elements[1].name
                field_File = self.filename + "_field_" + str(spec1) + str(spec2) + ".txt"
                homo_cor_File = self.filename + "_homo_cor_" + str(spec1) + str(spec2) + ".txt"
                instr += "field occ, {}, {}, {}, {}, {}\n".format(spec1, spec2, field_File, '0', '9')
                instr += "#homo cor, \"occ\", {}, {}, {}\n".format(spec1, spec2, homo_cor_File)
                instr += "\n"
            """

            combin = list(combinations(np.arange(1, len(nhood[0][0].species) + 1), 2))
            for pair in combin:
                spec1 = nhood[0][0].species.elements[pair[0] - 1].name
                spec2 = nhood[0][1].species.elements[pair[1] - 1].name
                elem_pairs = [spec1, spec2]
                # if species are the same, occ is between voids
                if elem_pairs in used_pairs: #or [spec2, spec1] in used_pairs:
                    continue
                if spec1 == spec2:
                    spec2 = 'VOID'
                field_File = self.filename + "_field_" + str(spec1) + str(spec2) + ".txt"
                homo_cor_File = self.filename + "_homo_cor_" + str(spec1) + str(spec2) + ".txt"
                instr += "corr occ, {}, {}\n".format(spec1, spec2)
                #instr += "field occ, {}, {}, {}, {}, {}\n".format(spec1, spec2, field_File, '0', str(self.cell_multiplier[0]))
                #instr += "homo cor, occ, {}, {}, {}\n".format(spec1, spec2, homo_cor_File)
                instr += "\n"
                used_pairs.append(elem_pairs)

        """
        multi_occ_sites = self.multi_occupancy_sites(structure = self.structure)
        for species in multi_occ_sites.composition.elements:
            spec = species.name
            homo_occ_File = self.filename + "_homo_occ_" + str(spec) + ".txt"
            instr += "homo occ, {}, {}\n".format(spec, homo_occ_File)
        """

        instr += "exit\n\n\n"
        return instr

    def discus_fourier_string(self):
        """ Generate the discus string for Fourier calculations. """

        instr = "fourier\n"

        instr += "ll {}, {}, {}\n".format(-self.hkl_max[0],
                                          -self.hkl_max[1],
                                          -self.hkl_max[2])
        instr += "lr {}, {}, {}\n".format( self.hkl_max[0],
                                          -self.hkl_max[1],
                                          -self.hkl_max[2])
        instr += "ul {}, {}, {}\n".format(-self.hkl_max[0],
                                           self.hkl_max[1],
                                          -self.hkl_max[2])
        instr += "tl {}, {}, {}\n".format(-self.hkl_max[0],
                                          -self.hkl_max[1],
                                           self.hkl_max[2])

        instr += "abs h\n"
        instr += "ord k\n"

        instr += "nabs {}\n".format(self.fou_grid_size[0])
        instr += "nord {}\n".format(self.fou_grid_size[1])
        instr += "ntop {}\n".format(self.fou_grid_size[2])

        instr += "temp ignore\n"

        instr += "disp off\n"

        # Set up lots
        instr += "set aver, 0\n"
        lot_size = np.ceil(self.cell_multiplier / 3)
        instr += "#lots box,{},{},{},all,yes\n".format(*lot_size)
        instr += "\nrun\n"

        instr += "exit\n"

        instr += "output\n"
        instr += "form hdf5\n"
        instr += "outf {}\n".format(self.filename + ".h5")
        instr += "value intensity\n"

        instr += "run\n"
        instr += "exit\n"

        return instr

    def write_discus_macro(self):
        """ Write a DISCUS macro to simulate disorder and calculate diffraction """
        outfile = os.path.join(self.outdir, self.filename + ".mac")
        with open(outfile, 'w') as f:
            f.write(self.discus_generate_string(save_stru = True, save_cif=True))

            f.write(self.discus_fourier_string())

            f.write("exit\nexit\n")



def plot_h5_3d(h5file, vmax=None):
    """ Basic 3D sliced plot for existing h5 data. """
    from mayavi import mlab
    import h5py

    f = h5py.File(h5file)
    m = np.max(f['data'])
    s = mlab.pipeline.scalar_field(f['data'][:])

    # mlab.pipeline.volume(ss, vmax=vmax)
    mlab.pipeline.image_plane_widget(s, plane_orientation='x_axes', slice_index=10, vmax=vmax)
    mlab.pipeline.image_plane_widget(s, plane_orientation='y_axes', slice_index=10, vmax=vmax)
    mlab.pipeline.image_plane_widget(s, plane_orientation='z_axes', slice_index=10, vmax=vmax)

    mlab.outline()

    f.close()


def achieved_correlations(discus_string):
    """ Extract correlation values achieved from discus output

    Returns
    -------

    correlations : dict
        dict of  correlations present. key is neighbourhood number, and each entry is
    """

    lines = discus_string.split('\n')

    correlations = {}

    try:
        mmc_start = lines.index(" discus/mmc> run")
    except ValueError:
        # No mmc performed - assume no correlations
        return correlations

    corr_summary = lines.index(' --- Final multiple energy configuration ---')
    corr_end = lines.index(' --- Average energy changes ---')

    for l in range(corr_summary, corr_end):
        parts = lines[l].split()
        if len(parts) == 0:
            continue
        if parts[0].isnumeric():
            if int(parts[0]) in correlations.keys():
                # neighbourhood already exists
                correlations[int(parts[0])]['corr'].append(float(parts[5]))
            else:
                correlations[int(parts[0])] = {'target': float(parts[4]), 'corr': [float(parts[5])]}
        elif parts[0] == 'Stagnant:' and parts[1] == 'largest':
            # Check if anything has changed during mmc
            if '***' in parts[3]:
                status = 'free'
            elif float(parts[3]) == 0.0:
                status = 'static'
            else:
                status = 'free'
            # Put this status in the dict
            for c in correlations:
                correlations[c]['status'] = status

    return correlations

def extract_discus_runs(output_file):
    """ Extract individual discus outputs from a multi-calculation file

    Returns
    -------

    parts : dict
        Dict (keyed by macro name) of discus output strings
    """

    with open(output_file) as f:
        output = f.read()

    chunks = []

    start = 0
    end = 0

    while start >= 0 and end >= 0:
        start = output.find('------ > Reading macro ', end)
        end =  output.find(' .....................................', start)

        if start >= 0 and end >= 0 and output[start : end].find('suite> exit') > 0:
            chunks.append((start, end))

    parts = {}
    for c in chunks:
        nameend = output[c[0]: c[1]].find(".mac")
        name = output[c[0]: c[1]][23 : nameend]
        parts[name] = output[c[0]: c[1]]

    return parts

def correlations_to_config(corrs, config):
    """ Update an existing config file to include achieved correlations. """
    import json

    with open(config) as f:
        conf = json.load(f)

    for struct in corrs:
        for neigh in corrs[struct]:
            for vec in conf[struct]['NH'+str(neigh)]:
                assert np.abs(float(conf[struct]['NH'+str(neigh)][vec]['corr']) - float(corrs[struct][neigh]['target'])) < 0.002
                conf[struct]['NH'+str(neigh)][vec]['achieved'] = corrs[struct][neigh]['corr']
                conf[struct]['NH'+str(neigh)][vec]['status'] = corrs[struct][neigh]['status']

    with open(config+'.update', 'w') as f:
        json.dump(conf, f)


def truth_from_conf(config_file, corr_threshold=0.2):
    """ Return a simplified ground truth from a config file. """

    with open(config_file, 'r') as f:
        config = json.load(f)

    corr_dims = {}

    for struct in config:
        ndims = 0
        for nhood in config[struct]:
            if 'achieved' not in config[struct][nhood]['V1'].keys():
                ndims = -1
                break
                #raise KeyError('Correlations have not been extracted for {}'.format(struct))
            corrs = np.array(config[struct][nhood]['V1']['achieved'])
            if (np.abs(corrs) > corr_threshold).any() and config[struct][nhood]['V1']['status'] == 'free':
                ndims += 1

        if ndims >= 0:
            corr_dims[struct] = ndims

    return corr_dims
    
def is_disordered(structure, rad='xray', min_scatter_diff = 10, occ_tol = 0.1):
    """ Return whether a pymatgen structure is disordered given the specified scattering difference.
    
    Parameters
    -----------
    
    structure : pymatgen Structure object
        Pymatgen Structure object to inspect
    rad : str, default 'xray'
        Type of scattering to be considered when determining differences (xray, neutron)
    min_scatter_diff : float
        Minimum amount of scattering difference required on a mixed site to count as disordered.
    occ_tol : float, default 0.1
        minimum amount of occupancy on site in order to be considered 'doped' from the view of diffraction
        
    returns
    -------
    
    is_disordered : bool
        Can the Structure be considered disordered
        
    Notes
    -----
    
    'Disorder' here depends on 
        1. The mixed occupancy of a site exceeding the threshold `occ_tol`
        2. The scattering difference between mixed site species being greater than `min_scatter_diff`
        
    Note that `is_disordered` will return True if just one site has sufficient disordered based on the 
    criteria, irrespective of other sites.
        - You may need to make sure that only this site is selected when setting up a calculation, for
          instance if another site has mixed occupancy but insufficient scattering difference.
    
    TODO:
        - Add ability to determine difference based on e.g. neutron scattering lengths.
    
    """
    
    if rad != 'xray':
        raise NotImplementedError('Currently, only X-ray-based disorder is implemented')
    
    if structure.is_ordered:
        # Has no mixed sites, so can safely be ignored
        return False
        
    for site in structure:
        if not site.is_ordered:
            el_dict = site.species.get_el_amt_dict()
            if len(el_dict) != 2:
                # Site has either 1 sub-occupied species or more than 2 species - skipping
                continue
            print('here')

            if max(el_dict.values()) < (1 - occ_tol):
                # Largest element is less than 0.9 occupied, so is disordered
                if rad == 'xray':
                    Z_values = [s.Z for s in site.species.elements]
                    if max(Z_values) - min(Z_values) >= min_scatter_diff:
                        return True

    return False
                
    

if __name__ == "__main__":

    import argparse
    import json
    import glob

    parser = argparse.ArgumentParser(description='Generate multiple macros and cell files')

    input_group = parser.add_mutually_exclusive_group(required=True)

    input_group.add_argument('-f', '--file', type=str,
                             help = "Path to JSON-formatted file containing CIF definitions")
    input_group.add_argument('-d', '--directory', type=str,
                             help = "Directory path containing multiple CIF files")

    input_group.add_argument('--discus_outs', type=str, nargs='+',
                             help = "Discus output file[s] to extract results from")


    parser.add_argument('-o', '--outdir', type=str, default='.',
                        help = "Directory to store output macros and cell files")

    parser.add_argument('-c','--corr', type=str, default='random',
                        help = 'Type of correlations to consider; "occ" or "disp" (default random, i.e. both)')

    parser.add_argument('-l', '--logfile', type=str, default = 'config.json',
                        help = 'File to save initial configurations to (default: config.json)')

    # -i parameter included for testing. If ignored then 'included' setting is assumed and this is written in outfile name.
    parser.add_argument('-i', '--inverse', type=str, default = 'included',
                        help = 'Whether the inverse vector is included in the same neigbourhood, a separate one, or not at all.')
                        
    parser.add_argument('-e', '--exclude-ordered', action='store_true',
                        help="Only generate macros for chemically disordered (e.g. mixed-occupancy) structures with different X-ray scattering lengths")
 


    args = parser.parse_args()

    assert args.corr in ['occ','disp','random']
    assert args.inverse in ['included', 'separate', 'none']


    if args.discus_outs:
        # Don't generate new files, but collect existing results.
        assert os.path.isfile(args.logfile)

        conf_update = {}
        print("loading {} discus output files".format(len(args.discus_outs)))
        for dis_out in tqdm(args.discus_outs):
            try:
                parts = extract_discus_runs(dis_out)
                for p in parts:
                    conf_update[p] = achieved_correlations(parts[p])
            except:
                print(dis_out)
                raise


        if len(conf_update) > 0:
            print("Updating config for {} structures".format(len(conf_update)))

            correlations_to_config(conf_update, args.logfile)
            print("Successfully updated correlation information")

            print('Extracting true correlations to truth.json')

            true_conf = truth_from_conf(args.logfile + '.update', corr_threshold = 0.2)


            conf_dir = os.path.dirname(args.logfile)
            with open(os.path.join(conf_dir, 'truth.json'), 'w') as f:
                json.dump(true_conf, f)

        else:
            print("No correlation obtained from discus output(s)")

        quit()


    if args.file:
        if os.path.splitext(args.file)[-1] == '.json':
            with open(args.file) as f:
                data = json.load(f)

            cifs = [i['cif'] for i in data]
        elif os.path.splitext(args.file)[-1] == '.cif':
            cifs = [args.file]

    elif args.directory:
        cifs = glob.glob( os.path.join(args.directory, '*.cif') )
        print("Processing {} cif files...".format(len(cifs)))
    config = {}

    #os.chdir(args.outdir)

    status = {'processed':[],
              'ordered':[],
              'skipped':[]}

    for dat in tqdm(cifs):
        #print(dat)
        #rng = np.random.default_rng(100)
        try:
            cryst = Disordered_Crystal.from_CIF(dat, outdir = args.outdir, inverse_state = args.inverse)
        except:
            print("Error generating crystal from {}, skipping".format(dat))
            status['skipped'].append(dat)
        
        if args.exclude_ordered:
            if not is_disordered(cryst.structure):
                print("Excluded {} as ordered".format(dat))
                status['ordered'].append(dat)
                continue
            
        
        dim = rng.choice([0,1,2])

        #print("DIM = ", dim)

        # If semi-occupied sites exist we'll use these for the correlations
        # Extract only the fractionally-occupied (or mixed-occ) sites for generating correlations
        multi_occ_sites = cryst.multi_occupancy_sites(structure = cryst.structure)
        
        # Select a single crystallographic site to form the correlations
        choice_arr = np.arange(0, len(multi_occ_sites), step = 1, dtype = int)
        ref_site_index = rng.choice(choice_arr)
        ref_site = multi_occ_sites.sites[ref_site_index]

        
        selected_structure = cryst.equiv_occupancy_sites(ref_site, structure = multi_occ_sites)
        #print("SEL STRU: ", selected_structure)


        if dim > 0:
            cryst.random_neighbourhood(ndim = dim, corr_type = args.corr, structure = selected_structure, inverse = args.inverse)

        config[cryst.filename] = {'DIM': str(dim)}
        #config[cryst.filename]['DIM'] = {'DIM': str(dim)}
        # Note, we're using discus-like counting (from 1) for vectors and neighbourhoods
        #print("~~~~~~~~~~~~~~~~~~~~~~~~~")
        #print(cryst.neighbourhoods)
        for i, nhood in enumerate(cryst.neighbourhoods):
            config[cryst.filename]['NH'+str(i+1)] = {}
            for j, vec in enumerate(nhood):
                idx1 = cryst.structure.index(vec[0])+1
                idx2 = cryst.structure.index(vec[1])+1
                config[cryst.filename]['NH'+str(i+1)]['V'+str(j+1)] = {'site1': str(idx1),
                                                                       'site2': str(idx2),
                                                                       'jimage': str(vec[2]),
                                                                       'corr' : str(vec[3])
                                                                      }

        try:
            cryst.write_discus_cell()
            cryst.write_discus_macro()
        except:
            print("Error writing cell/macro for {}".format(dat))
            raise

        status['processed'].append(dat)


    with open(os.path.join(args.outdir, args.logfile), 'w') as f:
        json.dump(config, f, indent=1)

    print("Details of processing are in 'status.json'")
    print("    {} CIFs processed successfully".format(len(status['processed'])))
    print("    {} CIFs skipped due to ordered structure".format(len(status['ordered'])))
    print("    {} CIFs failed to process".format(len(status['skipped'])))
    print("\n")

    with open(os.path.join(args.outdir, 'status.json'), 'w') as f:
        json.dump(status, f, indent=1)

    print("Finished generating macro files - now run 'discus_suite -macro CIFNAME.mac' to generate data. ")
